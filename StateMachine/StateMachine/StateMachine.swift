//
//  StateMachine.swift
//  Library
//
//  Created by Evgeniy Abashkin on 27/01/2019.
//

import RxCocoa
import RxSwift

public protocol StateType {
    typealias Action = (inout Self) -> Void
    mutating func reduce(action: Action)
}

// MARK: - Action factories

/// Create empty action
public func action<State: StateType>() -> ((inout State) -> Void) {
    return { _ in }
}

/// Create action with single argument
public func action<A, B>(
    block: @escaping ((inout A, B) -> Void)
) -> (B) -> ((inout A) -> Void) {
    return { argumentB in { argumentA in block(&argumentA, argumentB) } }
}

/// Create action with two arguments
public func action<A, B, C>(
    block: @escaping ((inout A, B, C) -> Void)
) -> (B, C) -> ((inout A) -> Void) {
    return { argumentB, argumentC in { argumentA in block(&argumentA, argumentB, argumentC) } }
}

/// Create set state property action
/// let action: Action = action(set:\.flag, true)
public func action<State: StateType, Value>(
    set path: WritableKeyPath<State, Value>,
    _ value: Value
) -> ((inout State) -> Void) {
    return { (state: inout State) in
        state[keyPath: path] = value
    }
}

/// Create set state property action
public func action<State: StateType, Value>(
    set path: WritableKeyPath<State, Value>
) -> (Value) -> ((inout State) -> Void) {
    return action { (state: inout State, value: Value) in
        state[keyPath: path] = value
    }
}

// MARK: -

open class Store<S: StateType> {
    private let disposeBag = DisposeBag()

    private let stateObservable: Observable<S>
    private let actionSubject: PublishSubject<S.Action> = .init()

    public init(
        state: S,
        scheduler: SchedulerType
    ) {
        stateObservable = actionSubject
            .observeOn(scheduler)
            .scan(state) { state, action in
                var state = state
                state.reduce(action: action)
                return state
            }
            .startWith(state)
            .share(replay: 1)

        run()
    }

    /// Process actions handling without external subscriptions
    private func run() {
        stateObservable
            .subscribe()
            .disposed(by: disposeBag)
    }

    /// Dispatch action to state
    open func dispatch(_ action: @escaping S.Action) {
        actionSubject.onNext(action)
    }

    /// Create closure dispatching action to state
    /// Warning: capture reference to store
    open func dispatcher() -> (@escaping S.Action) -> Void {
        return { action in self.dispatch(action) }
    }

    /// Create closure dispatching action to state
    /// Warning: closure capture store reference
    open func dispatcher(_ action: @escaping S.Action) -> () -> Void {
        return { self.dispatch(action) }
    }

    /// Create closure dispatching action to state
    /// Warning: closure capture store reference
    open func dispatcher<V>(_ action: @escaping ((V) -> S.Action)) -> (V) -> Void {
        return { value in self.dispatch(action(value)) }
    }

    open var state: Observable<S> {
        return stateObservable
    }
}

extension Store {
    open func stateTriggerAt(_ keyPath: KeyPath<S, Bool>) -> Observable<Void> {
        return state
            .filter { $0[keyPath: keyPath] }
            .map { _ in () }
    }

    open func stateUnwrapAt<T>(_ keyPath: KeyPath<S, T?>) -> Observable<T> {
        return state
            .map { $0[keyPath: keyPath] }
            .flatMapLatest { Observable.from(optional: $0) }
    }

    open func stateDistinctAt<T: Equatable>(_ keyPath: KeyPath<S, T>) -> Observable<T> {
        return state
            .map { $0[keyPath: keyPath] }
            .distinctUntilChanged()
    }

    open func stateDistinctUnwrapAt<T: Equatable>(_ keyPath: KeyPath<S, T?>) -> Observable<T> {
        return state
            .map { $0[keyPath: keyPath] }
            .flatMapLatest { Observable.from(optional: $0) }
            .distinctUntilChanged()
    }
}

/// Store state driver extensions
extension Store {
    open var stateDriver: Driver<S> {
        return state.asDriver()
    }

    open func stateDriverTriggerAt(_ keyPath: KeyPath<S, Bool>) -> Driver<Void> {
        return stateTriggerAt(keyPath).asDriver()
    }

    open func stateDriverUnwrapAt<T>(_ keyPath: KeyPath<S, T?>) -> Driver<T> {
        return stateUnwrapAt(keyPath).asDriver()
    }

    open func stateDriverDistinctAt<T: Equatable>(_ keyPath: KeyPath<S, T>) -> Driver<T> {
        return stateDistinctAt(keyPath).asDriver()
    }

    open func statDriverDistinctUnwrapAt<T: Equatable>(_ keyPath: KeyPath<S, T?>) -> Driver<T> {
        return stateDistinctUnwrapAt(keyPath).asDriver()
    }
}

/// Store renamed extensions
extension Store {
    @available(*, deprecated, renamed: "stateDriverTriggerAt")
    open func stateFlagDriver(_ keyPath: KeyPath<S, Bool>) -> Driver<Void> {
        return stateDriverTriggerAt(keyPath)
    }

    @available(*, deprecated, renamed: "stateDriverUnwrapAt")
    open func stateWrappedValueDriver<T>(_ keyPath: KeyPath<S, T?>) -> Driver<T> {
        return stateDriverUnwrapAt(keyPath)
    }

    @available(*, deprecated, renamed: "stateDriverDistinctAt")
    open func stateChangableValueDriver<T: Equatable>(_ keyPath: KeyPath<S, T>) -> Driver<T> {
        return stateDriverDistinctAt(keyPath)
    }

    @available(*, deprecated, renamed: "statDriverDistinctUnwrapAt")
    open func stateChangableWrappedValueDriver<T: Equatable>(_ keyPath: KeyPath<S, T?>) -> Driver<T> {
        return statDriverDistinctUnwrapAt(keyPath)
    }
}

extension Store {
    open func feedback(_ block: (Observable<S>) -> Observable<S.Action>) -> Disposable {
        return feedback { _ in [block(stateObservable)] }
    }

    open func feedback(_ block: (Observable<S>) -> [Observable<S.Action>]) -> Disposable {
        return Observable.from(block(stateObservable))
            .flatMap { $0 }
            .concat(Observable.never())
            .bind(to: actionSubject)
    }
}

extension ObservableConvertibleType {
    func asDriver() -> Driver<Self.E> {
        return asDriver { _ in .never() }
    }
}
